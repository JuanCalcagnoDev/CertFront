(function() {
  'use strict';

  angular
    .module('desktop', ['ngResource', 'ui.router', 'toastr']);

})();
